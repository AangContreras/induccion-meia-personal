# Mis actividades del curso de Inducción del Macroentrenamiento en Inteligencia Artificial

 Este repositorio es un clon del repositorio original:
 [Induccion_MeIA](https://github.com/jugernaut/Induccion_MeIA), pero con los
 ejercicios que hasta ahora yo he ido resolviendo, editando o mejorando como
 estudiante en el curso.